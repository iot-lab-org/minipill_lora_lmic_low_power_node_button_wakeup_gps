# MiniPill LoRa Hardware version 1.x - Button Wakeup and GPS for TTN mapper

Please look at https://www.iot-lab.org/blog/821/ for general information on this
project. In this this file I will share some software specific information.
All hardware versions 1.x are supported by this software

## Update 2022-07-28
Added thumbwheel switch to choose DataRate.
```
DataRate Table EU868
0 LoRa: SF12 / 125 kHz  250
1 LoRa: SF11 / 125 kHz  440
2 LoRa: SF10 / 125 kHz  980
3 LoRa: SF9 / 125 kHz   1760
4 LoRa: SF8 / 125 kHz   3125
5 LoRa: SF7 / 125 kHz   5470
6 LoRa: SF7 / 250 kHz   11000
7 FSK: 50 kbps          50000
8..15 RFU
```
The thumbwheel I had was able to count 0-6. Connected to Ground and PB8 (bit0), PB9 (bit1) and PB10 (bit2).

Changed debug output to Serial1a (Alternative Serial1) Tx on PB6.
Changed first packet will always be sent to start Join Process.


## Update 2022-01-09
Changed to OTAA activation because I could not prevent TTNv3 sending downlink messages with this
LMIC library
After reboot the join is done once, while power remains.

## Update 2021-12-30
Added code to read GPS info and using TinyGPSPlus library.
Take a while to get the serial communication right. You have to use the Alternative pins
to connect the GPS (PB7 <- Tx GPS). For debugging reasons the Serial2 is used.

Remember this is not a low power setup due to the GPS module. However, you can modify the
code yourself to set the GPS module in Low Power mode.
An u-blox 6 GPS Module is used for testing (GY-NEO6MV2)

When the button is hit and the GPS have no updated info, or lost connection, no LoRa package
will be send.

using the (realtime) os functions to avoid timing conflicts.
Serial debug data is available, please remove them if you do not need them.

## Update 2021-12-18
Updated STM32LowPower and STM32RTC due to updated STM32 framework for PlatformIO

## Wakeup with external button instead of timed wakeup
In this code I use the LMIC library and use a external switch with debounce circuit
to wake up the MiniPill Lora. The LowPower library uses attachInterruptWakeup to wakeup
on a designated pin.

The first message on boot is always send, due to OTAA handshake. This code can be
used with both ABP and OTAA activation.

Messages will not be send after every press on the button. There has to be a timeout between them.
In my experiments it took about 8 seconds for the controller to finish one button event. But due to the LoRaWAN regulations it is not advisable to send data with very little pause between them.
When the button is pushed multiple times in a short period, only 1 message will be send.

## Very low Power
The power consumption is about 0.5uA when in deepSleep mode, even lower than the 1.5uA
in timed wakeup. This value is when the GPS module is not connected. Hence the GPS module power is connected with a switch. Check the GPS module information for information on the power usage and low power settings (switching off by software). I did not implement this on this project.

/*
  main.cpp
  Main code where the control takes place
  @author  Leo Korbee (c), Leo.Korbee@xs4all.nl
  @website iot-lab.org
  @license Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
  Thanks to all the folks who contributed beforme me on this code or libraries.
  Please share you thoughts and comments or you projects with me through e-mail
  or comment on my website

  Please remove all comments on Serial.print() commands to get debug
  information on hardware serial port (9600 baud). Rember that this wil take
  about 2uA in Sleep Mode

  Hardware information at the end of this file.

  @version 2020-10-08
  Added code so you can use ABP instead of OTAA. Please enable DISABLE_JOIN
  in the config.h in the lmic library and change ABP config in secconfig.h

  ======== Wakup by switch/external interrupt ========

  @version 2020-12-18
  Changed the code so the device can wakeup from sleep when pressing a button.
  Checkout the hardware configuration at the end of this file. Please use a
  debounce circuit to avoid multiple interrupts occur at once.

  @version 2021-02-27
  No BME280 connected and using TTN Stack V3 with ABP

  @version 2021-03-07
  LMIC.rxDelay = 5; added in setup function to prevent downlink messages
  It should be 5 seconds, other values do not have the same effect!

  @version 2021-12-18 added GPS function to make it possible to use this device
  as GPS tracker for the TTNMAPPER.

  Hardware connections:
  Using GY-NEO6MV2 module
  NC = Rx connected to Tx GPS module
  PB7 = Tx connected to Rx GPS module
  Default Baud Rate = 9600.

  @version 2021-12-30
  using the right callbacks to the os and using interrupts to start new callback.

  @version 2022-01-08
  changed to OTAA activation, unable to prevent downlink packets with ABP

  @version 2022-07-28
  Debug output to Serial1a PB6. Serial2 does not work in this setting for
  unknown reasons. No conflict found with other hardware/software.



*/

#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include "STM32LowPower.h"
#include "STM32IntRef.h"
#include "TinyGPS++.h"

void do_send(osjob_t* j);

// for debugging redirect to hardware Serial2
// Tx on PA6
#define DEBUGDEV Serial1a
// HardwareSerial Serial2(USART2);   // or HardWareSerial Serial2 (PA3, PA2);

HardwareSerial Serial1a(PB7, PB6); // Using Alternative pins for Serial 1 Rx = PB7, Tx = PB6

// include security credentials OTAA, check secconfig_example.h for more information
#include "secconfig.h"

#ifdef DISABLE_JOIN
// These callbacks are only used in over-the-air activation, so they are
// left empty here. We cannot leave them out completely otherwise the linker will complain.
// DISABLE_JOIN is set in config.h for using ABP
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }
#else
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}
#endif

static osjob_t sendjob;
bool callback = false;

// use a button as ExternalWakeup
#define WKUP_BTN PB11

// led for signing a packet is send
#define SIGNAL_LED PA1

// uses wakeupcall, does nothing
void wakeupFunction()
{
  // This function will be called once on device wakeup
  // You can do some little operations here (like changing variables which will be used in the loop)
  // Remember to avoid calling delay() and long running functions since this functions executes in interrupt context
  DEBUGDEV.println(".");
  if (!callback)
  {
    callback = true;
    os_setTimedCallback(&sendjob, os_getTime()+ms2osticks(50), do_send);
  }
}

// Pin mapping for the MiniPill LoRa with the RFM95 LoRa chip
const lmic_pinmap lmic_pins =
{
  .nss = PA4,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = PA9,
  .dio = {PA10, PB4, PB5},
};

// The TinyGPS++ object
TinyGPSPlus gps;

// prototype definitions
void goto_sleep();
uint8_t getThumbwheel();

// event called by os on events
void onEvent (ev_t ev)
{
     DEBUGDEV.print(os_getTime());
     DEBUGDEV.print(": ");
     switch(ev) {
         case EV_SCAN_TIMEOUT:
             DEBUGDEV.println(F("EV_SCAN_TIMEOUT"));
             break;
         case EV_BEACON_FOUND:
             DEBUGDEV.println(F("EV_BEACON_FOUND"));
             break;
         case EV_BEACON_MISSED:
             DEBUGDEV.println(F("EV_BEACON_MISSED"));
             break;
         case EV_BEACON_TRACKED:
             DEBUGDEV.println(F("EV_BEACON_TRACKED"));
             break;
         case EV_JOINING:
             DEBUGDEV.println(F("EV_JOINING"));
             break;
         case EV_JOINED:
             DEBUGDEV.println(F("EV_JOINED"));

             // Disable link check validation (automatically enabled
             // during join, but not supported by TTN at this time).
             LMIC_setLinkCheckMode(0);
             break;
         case EV_RFU1:
             DEBUGDEV.println(F("EV_RFU1"));
             break;
         case EV_JOIN_FAILED:
             DEBUGDEV.println(F("EV_JOIN_FAILED"));
             break;
         case EV_REJOIN_FAILED:
             DEBUGDEV.println(F("EV_REJOIN_FAILED"));
             break;
             break;
         case EV_TXCOMPLETE:
             DEBUGDEV.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
             if (LMIC.txrxFlags & TXRX_ACK)
    //           Serial.println(F("Received ack"));
             if (LMIC.dataLen)
             {
    //           Serial.print(F("Received "));
    //           Serial.print(LMIC.dataLen);
    //           Serial.println(F(" bytes of payload"));
               // print data
               for (int i = 0; i < LMIC.dataLen; i++)
               {
    //             Serial.print(LMIC.frame[LMIC.dataBeg + i], HEX);
               }
    //           Serial.println();
             }
             goto_sleep();
             break;
         case EV_LOST_TSYNC:
             DEBUGDEV.println(F("EV_LOST_TSYNC"));
             break;
         case EV_RESET:
             DEBUGDEV.println(F("EV_RESET"));
             break;
         case EV_RXCOMPLETE:
             // data received in ping slot
             DEBUGDEV.println(F("EV_RXCOMPLETE"));
             break;
         case EV_LINK_DEAD:
             DEBUGDEV.println(F("EV_LINK_DEAD"));
             break;
         case EV_LINK_ALIVE:
             DEBUGDEV.println(F("EV_LINK_ALIVE"));
             break;
          default:
             DEBUGDEV.println(F("Unknown event"));
             break;
     }
}

//
void do_send(osjob_t* j)
{
  // setup serial to run clocks... after sleep (stop mode)
  //Serial1a.begin(9600);
  //Serial2.begin(9600);

  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND)
  {
         DEBUGDEV.println(F("OP_TXRXPEND, not sending"));
  } else
  {
    if (LMIC.seqnoUp == 0)
    {
      // first packet
      DEBUGDEV.println("First packet to start Join process");
      // Prepare upstream data transmission at the next possible time.
      uint8_t dataLength = 1;
      uint8_t data[dataLength];
      data[0] = 0xFF;
      LMIC_setTxData2(1, data, sizeof(data), 0);
      // signal with LED that data is queued
      digitalWrite(SIGNAL_LED, LOW);
      delay(1000);
      digitalWrite(SIGNAL_LED, HIGH);
    }
    else
    {
      // Get GPS data
      DEBUGDEV.println("Start to read GPS data");
      // one batch of info will be processed. 2 seconds should be read...
      unsigned long start = millis();
      do
      {
        while (Serial1a.available())
          gps.encode(Serial1a.read());
      } while (millis() - start < 2000);

      if (gps.location.isValid() && gps.location.isUpdated())
      {
        DEBUGDEV.print(F("position: "));
        DEBUGDEV.print(gps.location.lat(), 6); // double
        DEBUGDEV.print(F(", "));
        DEBUGDEV.println(gps.location.lng(), 6); // double
        DEBUGDEV.print(F("altitude: "));
        DEBUGDEV.println(gps.altitude.meters()); // double
        DEBUGDEV.print(F("hdop: "));
        DEBUGDEV.println(gps.hdop.hdop()); // double
        DEBUGDEV.print(F("number of satellites: "));
        DEBUGDEV.println(gps.satellites.value());

        // double = 4 bytes -> 32 bits...

        uint32_t lat = gps.location.lat() * 1000000;
        uint32_t lng = gps.location.lng() * 1000000;
        uint16_t alt = gps.altitude.meters() * 100;
        uint16_t hdop = gps.hdop.hdop() * 100;
        uint8_t numberOfSatellites = gps.satellites.value();

        // Prepare upstream data transmission at the next possible time.
        uint8_t dataLength = 15;
        uint8_t data[dataLength];

        // read vcc and add to bytebuffer
        int32_t vcc = IntRef.readVref();
        data[0] = (vcc >> 8) & 0xff;
        data[1] = (vcc & 0xff);

        data[2] = (lat >> 24) & 0xff;
        data[3] = (lat >> 16) & 0xff;
        data[4] = (lat >> 8) & 0xff;
        data[5] = lat & 0xff;

        data[6] = (lng >> 24) & 0xff;
        data[7] = (lng >> 16) & 0xff;
        data[8] = (lng >> 8) & 0xff;
        data[9] = lng & 0xff;

        data[10] = (alt >> 8) & 0xff;
        data[11] = alt & 0xff;

        data[12] = (hdop >> 8) & 0xff;
        data[13] = hdop & 0xff;

        data[14] = numberOfSatellites & 0xff;

        DEBUGDEV.println("set data");
        LMIC_setTxData2(1, data, sizeof(data), 0);
      //       Serial.println(F("Packet queued"));
        // signal with LED that data is queued
        digitalWrite(SIGNAL_LED, LOW);
        delay(1000);
        digitalWrite(SIGNAL_LED, HIGH);
      }
      else
      {
        // set data to zero to force not to send any data
        // uint8_t data[0];
        // LMIC_setTxData2(1, data, sizeof(data), 0);

        DEBUGDEV.println(F("INVALID or NOT UPDATED GPS data, no update send"));
        os_clearCallback(j);
        goto_sleep();
      }
    }
  }
}


void goto_sleep()
{
  DEBUGDEV.println("going to sleep");
  callback = false;
  // reset low power parameters, seems to work -> 0.6 uA without BME280
  LowPower.begin();
  delay(100);
  // sleep until interrupt occurs...
  LowPower.deepSleep();
  DEBUGDEV.println("waking up");
}

void setup()
{
  // setup for GPS and debugging
  DEBUGDEV.begin(9600);
  // delay at startup for debugging reasons
  delay(8000);
  DEBUGDEV.println(F("Starting node"));
  delay(1000);
  // Set pin as INPUT_PULLUP to avoid spurious wakeup and use it
  // for external debounce circuit.
  pinMode(WKUP_BTN, INPUT_PULLUP);

  // set pin as OUPUT for LED
  pinMode(SIGNAL_LED, OUTPUT);
  digitalWrite(SIGNAL_LED, HIGH);

  // setup for thumbwheel to set datarate thumbwheel set to ground
  // pinMode(PB8, INPUT_PULLUP);
  // pinMode(PB9, INPUT_PULLUP);
  // pinMode(PB10, INPUT_PULLUP);

  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();
  // to incrise the size of the RX window.
  LMIC_setClockError(MAX_CLOCK_ERROR * 10 / 100);

  // Set static session parameters when using ABP.
  // Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
  #ifdef DISABLE_JOIN
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
  // These settings are needed for correct communication. By removing them you
  // get empty downlink messages
  // Disable ADR
  LMIC_setAdrMode(0);
  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;
  // prevent some downlink messages, TTN advanced setup is set to the default 5
  LMIC.rxDelay = 5;
  // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7,14);
  #endif

  // Configure low power at startup
  LowPower.begin();

  // Attach a wakeup interrupt on pin, calling wakeupFunction when the device is woken up
  LowPower.attachInterruptWakeup(WKUP_BTN, wakeupFunction, RISING);


  //getThumbwheel();
  //LMIC_setDrTxpow(DR_SF7, 14);

  // Start job (sending automatically starts OTAA too, or first ABP message)
  os_setCallback(&sendjob, do_send);
}

void loop()
{
  // run the os_loop to check if a job is available
  os_runloop_once();
}


uint8_t getThumbwheel()
{
  // read bit 2, 1 and 0
  uint8_t data = (digitalRead(PB10) << 2) &
                 (digitalRead(PB9) << 1) &
                  digitalRead(PB8);
  DEBUGDEV.print("Thumbwheel: ");
  DEBUGDEV.print(data);
  return data;
}

/* MiniPill LoRa v1.x mapping - LoRa module RFM95W
  PA4  // SPI1_NSS   NSS  - RFM95W
  PA5  // SPI1_SCK   SCK  - RFM95W
  PA6  // SPI1_MISO  MISO - RFM95W
  PA7  // SPI1_MOSI  MOSI - RFM95W

  PA10 // USART1_RX  DIO0 - RFM95W
  PB4  //            DIO1 - RFM95W
  PB5  //            DIO2 - RFM95W

  PA9  // USART1_TX  RST  - RFM95W

  PB6 // USART1a_TX   RX - DEBUG device!
  PB7 // USART1a_RX   TX - GY-NEO6MV2 GPS module

  VCC - 3V3
  GND - GND

  PB8                BIT0 - Thumbwheel
  PB9                BIT1 - Thumbwheel
  PB10               BIT2 - Thumbwheel

  PB11 // switch connected to ground with little debounce circuit:

  Schematic:

  VCC
   |
  | | internal PULL_UP ristor, datasheet: about 40k
  | |
   |
  -+--- PB11 ---+---[ 4k7 ]----+
                |              |
                |              +- |
              _____               |-|
              _____ 100n       +- |
                |              |
  GND-----------+--------------+

*/
